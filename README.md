# README #

Apache POI Plugin by SR Software.

### What is this repository for? ###

* This is a simple plugin based on Apache POI and Java's Reflection API to construct a number of Bean class (simple bean) from an excel file.
* version: 1.0.


Please note: this is a maven based project.

### How do I get set up? ###

* install the JAR file locally. If you are using maven then in your pom add the dependency as 

```
#!java

    <groupId>com.srsoftware.poi.plugin</groupId>
    <artifactId>poi-plugin</artifactId>
    <version>1.0-SNAPSHOT</version>
```


* You can directly invoke the class as 
  
```
#!java

ReadExcelFile.getInstance().generateBeanFromExcel(MyBean.class, "/path/to/excel.xlsx", 0);
```

here 0 is the sheet index.
Please note: right at this point this plugin support primitive data type and their wrapper classes and enums. Also, your excel file header needs to be exactly same as the one you have mentioned in your bean file. For an example, if you have studentName in your bean as

private String studentName;

then in your excel the column that represents this field could be any of the followings


1. Student Name

2. StudentName

3. student name

  --- you can use any combination of a case here.





### Dependencies ###
Apache POI plugin and JUnit
 

### Contact ###

* [SR Softwares](mailto:sanku.sumit@gmail.com)