package com.srsoft.poi.plugin;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.IteratorUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author - Sumit Roy
 * @Created On - 9 Apr, 2017,1:24:07 PM
 */
public class ReadExcelFile {

    private static ReadExcelFile ref = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(ReadExcelFile.class);

    private ReadExcelFile() {
    }

    public static ReadExcelFile getInstance() {
        if (null == ref) {
            ref = new ReadExcelFile();
        }
        return ref;
    }

    /**
     *
     * @param <T> Class type of Java Bean class that represents the data
     * structure as of the excel file
     * @param clazz class variable of the source Java class
     * @param filePath file path where the excel file is located
     * @param sheetIndex specific sheet index to be used as data source
     * @return List of Java Bean class as supplied
     * @throws Exception
     */
    public <T> List<T> generateBeanFromExcel(Class<T> clazz, String filePath, Integer sheetIndex) throws Exception {
        InputStream is = ReadExcelFile.class.getResourceAsStream(filePath);
        XSSFWorkbook workbook = new XSSFWorkbook(is);
        XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
        Iterator<Row> iterator = sheet.iterator();
        List<Row> allDataList = IteratorUtils.toList(iterator);
        Row firstRow = allDataList.get(0);

        Iterator<Cell> cellIterator = firstRow.cellIterator();
        List<String> headerLine = new ArrayList<>();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            headerLine.add(cell.getStringCellValue());
        }
        List<String> classSetters = getDeclaredSetters(clazz);
        LOGGER.debug("Read header lines as: {}", headerLine);
        Map<String, String> headerSetterMap = new LinkedHashMap<>();
        String setterPrefix = "set";
        headerLine.forEach((eachHeader) -> {
            for (String eachClassSetter : classSetters) {
                eachHeader = eachHeader.replaceAll(" ", "");
                if (eachClassSetter.toLowerCase().contains(setterPrefix + eachHeader.toLowerCase())) {
                    headerSetterMap.put(eachHeader, eachClassSetter);
                }
            }
        });
        LOGGER.debug("Lookup map for headers: {}", headerSetterMap);
        List<Map<String, Cell>> lookupMapList = new ArrayList<>();
        List<String> headersArr = new ArrayList<>(headerSetterMap.keySet());
        for (int s = 1; s < allDataList.size(); s++) {
            Map<String, Cell> lookupMap = new LinkedHashMap<>();
            Iterator<Cell> valueRow = allDataList.get(s).cellIterator();
            int i = 0;
            while (valueRow.hasNext()) {
                Cell cell = valueRow.next();
                String setMethodName = headerSetterMap.get(headersArr.get(i));
                lookupMap.put(setMethodName, cell);
                i++;
            }
            lookupMapList.add(lookupMap);
        }
        LOGGER.debug("Lookup map as: {}", lookupMapList);
        return extractDataFromExcel(clazz, lookupMapList);
    }

    private <T> List<String> getDeclaredSetters(Class<T> clazz) {        
        Method[] declaredMethods = clazz.getDeclaredMethods();
        List<String> setterMethodList = new ArrayList<>();
        for (Method eachDM : declaredMethods) {
            String methodName = eachDM.getName();
            if (methodName.startsWith("set")) {
                setterMethodList.add(methodName);
            }
        }
        return setterMethodList;
    }

    private <T> List<T> extractDataFromExcel(Class<T> clazz, List<Map<String, Cell>> lookupMapList) throws Exception {
        List<T> newInstanceList = new ArrayList<>();
        for (Map<String, Cell> eachMap : lookupMapList) {
            T newInstance = clazz.newInstance();
            for (String setterMethodName : eachMap.keySet()) {
                Cell cell = eachMap.get(setterMethodName);
                Method declaredSetterMethod;
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        String valueString = cell.getStringCellValue();
                        String getterMethodNameStr = setterMethodName.replaceFirst("set", "get");
                        Method getterOneStr = clazz.getDeclaredMethod(getterMethodNameStr);
                        Class<?> getterReturnTypeStr = getterOneStr.getReturnType();
                        if (getterReturnTypeStr.isEnum()) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, getterReturnTypeStr);
                            declaredSetterMethod.invoke(newInstance, Enum.valueOf((Class<Enum>) getterReturnTypeStr, valueString));
                        } else {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, String.class);
                            declaredSetterMethod.invoke(newInstance, valueString);
                        }
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        double val = cell.getNumericCellValue();
                        String getterMethodName = setterMethodName.replaceFirst("set", "get");
                        Method getterOne = clazz.getDeclaredMethod(getterMethodName);
                        Class<?> getterReturnType = getterOne.getReturnType();
                        if (getterReturnType == Long.class) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, Long.class);
                            declaredSetterMethod.invoke(newInstance, (long) val);
                        } else if (getterReturnType == Double.class) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, Double.class);
                            declaredSetterMethod.invoke(newInstance, val);
                        } else if (getterReturnType == Integer.class) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, Integer.class);
                            declaredSetterMethod.invoke(newInstance, (int) val);
                        } else if (getterReturnType == String.class) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, String.class);
                            declaredSetterMethod.invoke(newInstance, String.valueOf(val));
                        } else if (getterReturnType == Float.class) {
                            declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, String.class);
                            declaredSetterMethod.invoke(newInstance, (float) val);
                        }
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        Boolean valueBoolean = cell.getBooleanCellValue();
                        String getterMethodNameBool = setterMethodName.replaceFirst("set", "get");
                        if (!classHasMethod(clazz, getterMethodNameBool)) {
                            getterMethodNameBool = setterMethodName.replaceFirst("set", "is");
                        }
                        Method setterOneBool = clazz.getDeclaredMethod(getterMethodNameBool);
                        declaredSetterMethod = clazz.getDeclaredMethod(setterMethodName, setterOneBool.getReturnType());
                        declaredSetterMethod.invoke(newInstance, valueBoolean);
                        break;
                    default:
                }
            }
            newInstanceList.add(newInstance);
        }
        return newInstanceList;
    }

    private <T> boolean classHasMethod(Class<T> clazz, String getterMethodName) {
        boolean status = false;
        try {
            clazz.getDeclaredMethod(getterMethodName);
            status = true;
        } catch (NoSuchMethodException | SecurityException ex) {
        }
        return status;
    }
}
