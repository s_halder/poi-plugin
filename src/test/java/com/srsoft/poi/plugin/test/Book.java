package com.srsoft.poi.plugin.test;

/**
 *
 * @author - Sumit Roy
 * @Created On - 26 Mar, 2017,12:46:19 PM
 * @Project - LearningLambdaExp
 */
public class Book {

    private String bookName;
    private Long bookID;
    private Double bookCostPrice;
    private String isbnNumber;
    private Boolean eBookAvailable;

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Long getBookID() {
        return bookID;
    }

    public void setBookID(Long bookID) {
        this.bookID = bookID;
    }

    public Double getBookCostPrice() {
        return bookCostPrice;
    }

    public void setBookCostPrice(Double bookCostPrice) {
        this.bookCostPrice = bookCostPrice;
    }

    public String getIsbnNumber() {
        return isbnNumber;
    }

    public void setIsbnNumber(String isbnNumber) {
        this.isbnNumber = isbnNumber;
    }

    public Boolean iseBookAvailable() {
        return eBookAvailable;
    }

    public void seteBookAvailable(Boolean eBookAvailable) {
        this.eBookAvailable = eBookAvailable;
    }

    @Override
    public String toString() {
        return "Book{" + "bookName=" + bookName + ", bookID=" + bookID + ", bookCostPrice=" + bookCostPrice + ", isbnNumber=" + isbnNumber + ", eBookAvailable=" + eBookAvailable + '}';
    }

     

}
