package com.srsoft.poi.plugin.test;

import com.srsoft.poi.plugin.ReadExcelFile;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author - Sumit Roy
 * @Created On - 10 Apr, 2017,11:25:03 AM
 * @Project - poi-plug-in
 */
public class TestBeanGenerator {

    @Test
    public void testBeanGeneratorPlugin() throws Exception {
        String filePath = "/students.xlsx";
        List<Book> bookList = ReadExcelFile.getInstance().generateBeanFromExcel(Book.class, filePath, 0);
        bookList.forEach(System.out::println);
    }
}
